<?php

add_action( 'admin_init', 'theme_options_init' );
add_action( 'admin_menu', 'theme_options_add_page' );

/**
 * Init plugin options to white list our options
 */
function theme_options_init(){
	register_setting( 'sample_options', 'sample_theme_options', 'theme_options_validate' );
}

/**
 * Load up the menu page
 */
function theme_options_add_page() {
	add_theme_page( __( 'Theme footer edit', 'sampletheme' ), __( 'Theme footer edit', 'sampletheme' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
}

/**
 * Create arrays for our select and radio options
 */
$select_options = array(
	'blue' => array(
		'value' =>	'blue',
		'label' => __( 'blue', 'sampletheme' )
		),
	'pink' => array(
		'value' =>	'pink',
		'label' => __( 'pink', 'sampletheme' )
		),
	'purple' => array(
		'value' => 'purple',
		'label' => __( 'purple', 'sampletheme' )
		),
	'tomato' => array(
		'value' => 'tomato',
		'label' => __( 'tomato', 'sampletheme' )
		)
	);

$radio_options = array(
	'yes' => array(
		'value' => 'yes',
		'label' => __( 'Yes', 'sampletheme' )
		),
	'no' => array(
		'value' => 'no',
		'label' => __( 'No', 'sampletheme' )
		),
	'maybe' => array(
		'value' => 'maybe',
		'label' => __( 'Maybe', 'sampletheme' )
		)
	);

/**
 * Create the options page
 */
function theme_options_do_page() {
	global $select_options, $radio_options;

	if ( ! isset( $_REQUEST['settings-updated'] ) )
		$_REQUEST['settings-updated'] = false;

	?>
	<div class="wrap">
		<?php screen_icon(); echo "<h2>" . get_current_theme() . __( ': Edit', 'sampletheme' ) . "</h2>"; ?>

		<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
			<div class="updated fade"><p><strong><?php _e( 'Options saved', 'sampletheme' ); ?></strong></p></div>
		<?php endif; ?>
		<form method="post" action="options.php">
			<?php settings_fields( 'sample_options' ); ?>
			<?php $options = get_option( 'sample_theme_options' ); ?>

			<table class="form-table">

				<?php
				/**
				 * A sample text input option
				 */
				?>
				<tr valign="top"><th scope="row"><?php _e( 'Логотип партнера 1', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[logo1]" class="regular-text" type="text" name="sample_theme_options[logo1]" value="<?php esc_attr_e( $options['logo1'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Посилання на партнера 1', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[link1]" class="regular-text" type="text" name="sample_theme_options[link1]" value="<?php esc_attr_e( $options['link1'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Логотип партнера 2', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[logo2]" class="regular-text" type="text" name="sample_theme_options[logo2]" value="<?php esc_attr_e( $options['logo2'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Посилання на партнера 2', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[link2]" class="regular-text" type="text" name="sample_theme_options[link2]" value="<?php esc_attr_e( $options['link2'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Логотип партнера 3', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[logo3]" class="regular-text" type="text" name="sample_theme_options[logo3]" value="<?php esc_attr_e( $options['logo3'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Посилання на партнера 3', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[link3]" class="regular-text" type="text" name="sample_theme_options[link3]" value="<?php esc_attr_e( $options['link3'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Логотип партнера 4', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[logo4]" class="regular-text" type="text" name="sample_theme_options[logo4]" value="<?php esc_attr_e( $options['logo4'] ); ?>" />
					</td>
				</tr>
				<tr valign="top"><th scope="row"><?php _e( 'Посилання на партнера 4', 'sampletheme' ); ?></th>
					<td>
						<input id="sample_theme_options[link4]" class="regular-text" type="text" name="sample_theme_options[link4]" value="<?php esc_attr_e( $options['link4'] ); ?>" />
					</td>
				</tr>


				

				<?php
				/**
				 * A sample select input option
				 */
				?>
				
			</table>

			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e( 'Save Options', 'sampletheme' ); ?>" />
			</p>
		</form>
	</div>
	<?php
}

/**
 * Sanitize and validate input. Accepts an array, return a sanitized array.
 */
function theme_options_validate( $input ) {
	global $select_options, $radio_options;

	// Our checkbox value is either 0 or 1
	if ( ! isset( $input['option1'] ) )
		$input['option1'] = null;
	$input['option1'] = ( $input['option1'] == 1 ? 1 : 0 );

	// Say our text option must be safe text with no HTML tags
	$input['sometext'] = wp_filter_nohtml_kses( $input['sometext'] );

	// Our select option must actually be in our array of select options
	if ( ! array_key_exists( $input['selectinput'], $select_options ) )
		$input['selectinput'] = null;

	// Our radio option must actually be in our array of radio options
	if ( ! isset( $input['radioinput'] ) )
		$input['radioinput'] = null;
	if ( ! array_key_exists( $input['radioinput'], $radio_options ) )
		$input['radioinput'] = null;

	// Say our textarea option must be safe text with the allowed tags for posts
	$input['sometextarea'] = wp_filter_post_kses( $input['sometextarea'] );

	return $input;
}

// adapted from 	