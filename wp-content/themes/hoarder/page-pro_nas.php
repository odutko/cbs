<?php get_header(); ?>
<style>
 #map-canvas {
  height: 500px;
  margin: 0px;
  padding: 0px
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>

<script>

// This example displays a marker at the center of Australia.
// When the user clicks the marker, an info window opens.

/*Coordinats*/
function initialize() {
 var myLatlng = new google.maps.LatLng(49.846708,24.0276506);
 var myNumberFive = new google.maps.LatLng(49.833202,24.0349335);
 var myNumberSix = new google.maps.LatLng(49.836595,24.0028116);
 var myNumberSeven = new google.maps.LatLng(49.850267,24.0063225);
 var myNumberEight = new google.maps.LatLng(49.8359695,24.0170635);
 var myNumberFourteen = new google.maps.LatLng(49.8343995,23.8917554);
 var myNumberFifteen = new google.maps.LatLng(49.8451784,23.9685518);
 var myNumberSixteen = new google.maps.LatLng(49.85066,24.057811);
 var myNumberSeventeen = new google.maps.LatLng(49.8231781,23.9274234);
 var myNumberEightteen = new google.maps.LatLng(49.8231735,24.0308194);
 var myNumberTwentyThree = new google.maps.LatLng(49.816156,24.0406875);
 var myNumberTwentyFive = new google.maps.LatLng(49.8197486,24.020282);
 var myNumberTwentyEight = new google.maps.LatLng(49.9004803,23.9425241);
 var myNumberThirtyOne = new google.maps.LatLng(49.8620375,23.9659495);
 var myNumberThirtyTwo = new google.maps.LatLng(49.8133615,23.9798591);
 var myNumberThirtyThree = new google.maps.LatLng(49.8011769,24.0250151);
 var myNumberThirtyFour = new google.maps.LatLng(49.8174847,24.1469735);
 var myNumberThirtySeven = new google.maps.LatLng(49.818916,24.1361605);
 var myNumberThirtyEight = new google.maps.LatLng(49.8393645,24.0669099);
 var myNumberFourtyZero = new google.maps.LatLng(49.8198505,24.056226);
 var myNumberFourtyOne = new google.maps.LatLng(49.8382756,23.87254);
 var myNumberFourtyThree = new google.maps.LatLng(49.8197486,24.020282);
 var myNumberFourtyFour = new google.maps.LatLng(49.8197171,23.9840509);

 /*Center Zoom*/
 var mapOptions = {
  zoom: 11,
  zoom: 11,
  zoomControl: false,
  scaleControl: false,
  scrollwheel: false,
  center: myLatlng
};

var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

/*1*/
var contentStringOne = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">ЦМБ ім.Лесі Українки</h1>'+
'<p class="current_adrees"></p>' +
'<div class="detsad"><img src="<?php echo get_template_directory_uri(); ?>/images/DSC03410.JPG" alt="" /></div>' +
'<p>1932 р. - збудовано (за проектом архітекторів М. Вайца і Ю. Келльюра) <br>' +
'1940 - районна бібліотека <br>' +
'1947 - Центральна міська бібліотека <br>' +
'1949 - присвоєно ім"я Лесі Українки</p>' +
'<a href="" >Ми в Facebook</a>' +
'<p class="number">тел:255-33-38</p>' +
'</div>';

/*5*/
var contentStringFive = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №5 ім.А.Церетелі</h1>'+
'<p class="current_adrees">Вул. Шота-Руставелі,8</p>' +
'<a href="" >Ми в Facebook</a>' +
'<p class="number">тел:276-33-59  </p>' +

'</div>';

/*6*/
var contentStringSix = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №6</h1>'+
'<p class="current_adrees">Вул. Городоцька, 139</p>' +
'<a href="https://www.facebook.com/library6?fref=ts" >Ми в Facebook</a>' +
'<p class="number">тел: 238-22-38 </p>' +

'</div>';

/*7*/
var contentStringSeven = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №7</h1>'+
'<p class="current_adrees">Вул. Золота, 40</p>' +
'<a href="https://www.facebook.com/profile.php?id=100006921491024&fref=ts" >Ми в Facebook</a>' +
'<p class="number">тел: 233-34-46</p>' +

'</div>';

/*8*/
var contentStringEight = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №8 (Урбан бібліотека)</h1>'+
'<p class="current_adrees">Вул. Устияновича, 4</p>' +
'<a href="https://www.facebook.com/urbanbibliotekalviv?fref=ts" >Ми в Facebook</a>' +
'<p class="number">тел: 261-08-28 </p>' +

'</div>';

/*Fourteen*/
var contentStringFourteen = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №14</h1>'+
'<p class="current_adrees">Вул. Мазепи, 11</p>' +
'<a href="https://www.facebook.com/profile.php?id=100008355163881&fref=ts" >Ми в Facebook</a>' +
'<p class="number">тел:294-51-95  </p>' +

'</div>';

/*Fifteen*/
var contentStringFifteen = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №15</h1>'+
'<p class="current_adrees">Вул. Повітряна, 88</p>' +
'<a href="https://www.facebook.com/library15?fref=ts" >Ми в Facebook</a>' +
'<a href="http://15biblioteka.blogspot.com/" >Офіційний сайт</a>' +
'<p class="number">тел: 267-06-82  </p>' +
'</div>';

/*Sixteen*/
var contentStringSixteen = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №16</h1>'+
'<p class="current_adrees">Вул. Старознесенська, 56</p>' +
'<a href="https://www.facebook.com/profile.php?id=100005491815035&fref=ts" >Ми в Facebook</a>' +
'<p class="number">тел: 52-74-05 </p>' +

'</div>';

/*Seventeen*/
var contentStringSeventeen = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №17</h1>'+
'<p class="current_adrees">Вул. Городоцька, 281</p>' +
'<a href="https://www.facebook.com/profile.php?id=100004545315572&fref=ts" >Ми в Facebook</a>' +
'<p class="number">тел:262-22-21 </p>' +

'</div>';

/*Eightteen*/
var contentStringEightteen = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №18 (Сенсотека)</h1>'+
'<p class="current_adrees">Вул. Самчука, 22</p>' +
'<a href="https://www.facebook.com/profile.php?id=100007774059394&fref=ts" >Ми в Facebook</a>' +
'<a href="http://biblioteka18.blogspot.com/" >Офіційний сайт</a>' +
'<p class="number">тел:243-01-68  </p>' +
'</div>';

/*TwentyThree*/
var contentStringTwentyThree = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №23</h1>'+
'<p class="current_adrees">Вул. Тернопільська, 9</p>' +
'<a href="https://www.facebook.com/lastivkalviv?fref=ts" >Ми в Facebook</a>' +
'<p class="number">тел:270-23-74  </p>' +

'</div>';

/*TwentyFive*/
var contentStringTwentyFive = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №25</h1>'+
'<p class="current_adrees">Вул. Стрийська, 79</p>' +
'<a href="" >Ми в Facebook</a>' +
'<p class="number">тел:234-30-10  </p>' +

'</div>';

/*TwentyEight*/
var contentStringTwentyEight = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №28</h1>'+
'<p class="current_adrees">Вул. Незалежності України 31/Брюховичі/</p>' +
'<a href="https://www.facebook.com/profile.php?id=100007042778303&fref=ts" >Ми в Facebook</a>' +
'<a href="http://biblio28lv.blogspot.com/" >Офіційний сайт</a>' +
'<p class="number">тел: 234-68-57 </p>' +
'</div>';

/*ThirtyOne*/
var contentStringThirtyOne = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №31</h1>'+
'<p class="current_adrees">Вул. Шевченка, 270</p>' +
'<a href="https://www.facebook.com/profile.php?id=100008392543034&fref=ts" >Ми в Facebook</a>' +
'<p class="number">тел:291-39-15 </p>' +

'</div>';

/*ThirtyTwo*/
var contentStringThirtyTwo = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №32</h1>'+
'<p class="current_adrees">Вул. Виговського, 79</p>' +
'<a href="https://www.facebook.com/profile.php?id=100005246656224&fref=ts" >Ми в Facebook</a>' +
'<p class="number">тел:292-85-24</p>' +

'</div>';

/*ThirtyThree*/
var contentStringThirtyThree = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №33</h1>'+
'<p class="current_adrees">Вул. Хуторівка, 24</p>' +
'<a href="https://www.facebook.com/library33?fref=ts" >Ми в Facebook</a>' +
'<a href="http://bibliotekalviv.blogspot.com/" >Офіційний сайт</a>' +
'<p class="number">тел: 222-22-93 </p>' +
'</div>';

/*ThirtyFour*/
var contentStringThirtyFour = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №34/Винники/</h1>'+
'<p class="current_adrees">Вул. Галицька, 49</p>' +
'<a href="https://www.facebook.com/andruhiviryna?fref=ts" >Ми в Facebook</a>' +
'<p class="number">тел: 296-06-47  </p>' +

'</div>';

/*ThirtySeven*/
var contentStringThirtySeven = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №37</h1>'+
'<p class="current_adrees"></p>' +
'<a href="https://www.facebook.com/profile.php?id=100006626537031&fref=ts" >Ми в Facebook</a>' +
'<a href="https://www.facebook.com/libindahood?fref=ts" >Офіційний сайт</a>' +
'<p class="number">тел: 294-40-13 </p>' +

'</div>';

/*ThirtyEight*/
var contentStringThirtyEight = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №38</h1>'+
'<p class="current_adrees">Вул. Ніщинського, 27</p>' +
'<a href="https://www.facebook.com/orestarugok?fref=ts" >Ми в Facebook</a>' +
'<p class="number">тел:224-00-99  </p>' +

'</div>';

/*FourtyZero*/
var contentStringFourtyZero = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №40</h1>'+
'<p class="current_adrees">Вул. Зелена, 130</p>' +
'<a href="https://www.facebook.com/profile.php?id=100007035755723&fref=ts" >Ми в Facebook</a>' +
'<p class="number">тел: 270-26-34 </p>' +

'</div>';

/*FourtyOne*/
var contentStringFourtyOne = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №41</h1>'+
'<p class="current_adrees">Вул. Грушевського, 70/Рудно/</p>' +
'<a href="https://www.facebook.com/profile.php?id=100007118264847&fref=ts" >Ми в Facebook</a>' +
'<p class="number">тел: 243-01-69  </p>' +

'</div>';

/*FourtyThree*/
var contentStringFourtyThree = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Бібліотека-філія №43</h1>'+
'<p class="current_adrees">Вул. Стрийська, 79</p>' +
'<a href="https://www.facebook.com/profile.php?id=100005071877683&fref=ts" >Ми в Facebook</a>' +
'<a href="http://bibliotekafilia43.blogspot.com/" >Офіційний сайт</a>' +
'<p class="number">тел:234-34-85  </p>' +

'</div>';

/*FourtyFour*/
var contentStringFourtyFour = '<div id="adress_wrapp">'+
'<div id="siteNotice">'+
'</div>'+
'<h1 id="firstHeading" class="firstHeading">Ф.№44 вул.С.Петлюри, 43-21</h1>'+
'<p class="current_adrees">Вул. Петлюри, 43</p>' +
'<a href="https://www.facebook.com/profile.php?id=100008377748151&fref=ts" >Ми в Facebook</a>' +
'<p class="number">тел: 292-04-13 </p>' +

'</div>';
function closeClick() {
  infowindowOne.close();
  infowindowFive.close();
  infowindowSix.close();
  infowindowSeven.close();
  infowindowEight.close();
  infowindowFourteen.close();
  infowindowFifteen.close();
  infowindowSixteen.close();
  infowindowSeventeen.close();
  infowindowEightteen.close();
  infowindowTwentyThree.close();
  infowindowTwentyFive.close();
  infowindowTwentyEight.close();
  infowindowThirtyOne.close();
  infowindowThirtyTwo.close();
  infowindowThirtyThree.close();
  infowindowThirtyFour.close();
  infowindowThirtySeven.close();
  infowindowThirtyEight.close();
  infowindowFourtyZero.close();
  infowindowFourtyOne.close();
  infowindowFourtyThree.close();
  infowindowFourtyFour.close();
}
/*1*/
var infowindowOne = new google.maps.InfoWindow({
  content: contentStringOne
});
var markerOne = new google.maps.Marker({
  position: myLatlng,
  map: map,
  title: 'ЦМБ ім.Лесі Українки'
});
google.maps.event.addListener(markerOne,'click', function() {
  closeClick();
  infowindowOne.open(map,markerOne);
});

/*5*/
var infowindowFive = new google.maps.InfoWindow({
  content: contentStringFive
});
var markerFive = new google.maps.Marker({
  position: myNumberFive,
  map: map,
  title: 'Бібліотека-філія №5 ім.А.Церетелі'
});
google.maps.event.addListener(markerFive,'click', function() {
  closeClick();
  infowindowFive.open(map,markerFive);
});

/*6*/
var infowindowSix = new google.maps.InfoWindow({
  content: contentStringSix
});
var markerSix = new google.maps.Marker({
  position: myNumberSix,
  map: map,
  title: 'Бібліотека-філія №6'
});
google.maps.event.addListener(markerSix,'click', function() {
  closeClick();
  infowindowSix.open(map,markerSix);
});

/*7*/
var infowindowSeven = new google.maps.InfoWindow({
  content: contentStringSeven
});
var markerSeven = new google.maps.Marker({
  position: myNumberSeven,
  map: map,
  title: 'Бібліотека-філія №7'
});
google.maps.event.addListener(markerSeven,'click', function() {
  closeClick();
  infowindowSeven.open(map,markerSeven);
});

/*8*/
var infowindowEight = new google.maps.InfoWindow({
  content: contentStringEight
});
var markerEight = new google.maps.Marker({
  position: myNumberEight,
  map: map,
  title: 'Бібліотека-філія №8 (Урбан бібліотека)'
});
google.maps.event.addListener(markerEight,'click', function() {
  closeClick();
  infowindowEight.open(map,markerEight);
});

/*14*/
var infowindowFourteen = new google.maps.InfoWindow({
  content: contentStringFourteen
});
var markerFourteen = new google.maps.Marker({
  position: myNumberFourteen,
  map: map,
  title: 'Бібліотека-філія №14'
});
google.maps.event.addListener(markerFourteen,'click', function() {
  closeClick();
  infowindowFourteen.open(map,markerFourteen);
});

/*Fifteen*/
var infowindowFifteen = new google.maps.InfoWindow({
  content: contentStringFifteen
});
var markerFifteen = new google.maps.Marker({
  position: myNumberFifteen,
  map: map,
  title: 'Бібліотека-філія №15'
});
google.maps.event.addListener(markerFifteen,'click', function() {
  closeClick();
  infowindowFifteen.open(map,markerFifteen);
});

/*Sixteen*/
var infowindowSixteen = new google.maps.InfoWindow({
  content: contentStringSixteen
});
var markerSixteen = new google.maps.Marker({
  position: myNumberSixteen,
  map: map,
  title: 'Бібліотека-філія №16'
});
google.maps.event.addListener(markerSixteen,'click', function() {
  closeClick();
  infowindowSixteen.open(map,markerSixteen);
});

/*Seventeen*/
var infowindowSeventeen = new google.maps.InfoWindow({
  content: contentStringSeventeen
});
var markerSeventeen = new google.maps.Marker({
  position: myNumberSeventeen,
  map: map,
  title: 'Бібліотека-філія №17'
});
google.maps.event.addListener(markerSeventeen,'click', function() {
  closeClick();
  infowindowSeventeen.open(map,markerSeventeen);
});

/*Eightteen*/
var infowindowEightteen = new google.maps.InfoWindow({
  content: contentStringEightteen
});
var markerEightteen = new google.maps.Marker({
  position: myNumberEightteen,
  map: map,
  title: 'Бібліотека-філія №18 (Сенсотека)'
});
google.maps.event.addListener(markerEightteen,'click', function() {
  closeClick();
  infowindowEightteen.open(map,markerEightteen);
});

/*TwentyThree*/
var infowindowTwentyThree = new google.maps.InfoWindow({
  content: contentStringTwentyThree
});
var markerTwentyThree = new google.maps.Marker({
  position: myNumberTwentyThree,
  map: map,
  title: 'Бібліотека-філія №23'
});
google.maps.event.addListener(markerTwentyThree,'click', function() {
  closeClick();
  infowindowTwentyThree.open(map,markerTwentyThree);
});

/*TwentyFive*/
var infowindowTwentyFive = new google.maps.InfoWindow({
  content: contentStringTwentyFive
});
var markerTwentyFive = new google.maps.Marker({
  position: myNumberTwentyFive,
  map: map,
  title: 'Бібліотека-філія №25'
});
google.maps.event.addListener(markerTwentyFive,'click', function() {
  closeClick();
  infowindowTwentyFive.open(map,markerTwentyFive);
});

/*TwentyEight*/
var infowindowTwentyEight = new google.maps.InfoWindow({
  content: contentStringTwentyEight
});
var markerTwentyEight = new google.maps.Marker({
  position: myNumberTwentyEight,
  map: map,
  title: 'Бібліотека-філія №28'
});
google.maps.event.addListener(markerTwentyEight,'click', function() {
  closeClick();
  infowindowTwentyEight.open(map,markerTwentyEight);
});

/*ThirtyOne*/
var infowindowThirtyOne = new google.maps.InfoWindow({
  content: contentStringThirtyOne
});
var markerThirtyOne = new google.maps.Marker({
  position: myNumberThirtyOne,
  map: map,
  title: 'Бібліотека-філія №31'
});
google.maps.event.addListener(markerThirtyOne,'click', function() {
  closeClick();
  infowindowThirtyOne.open(map,markerThirtyOne);
});

/*ThirtyTwo*/
var infowindowThirtyTwo = new google.maps.InfoWindow({
  content: contentStringThirtyTwo
});
var markerThirtyTwo = new google.maps.Marker({
  position: myNumberThirtyTwo,
  map: map,
  title: 'Бібліотека-філія №32'
});
google.maps.event.addListener(markerThirtyTwo,'click', function() {
  closeClick();
  infowindowThirtyTwo.open(map,markerThirtyTwo);
});

/*ThirtyThree*/
var infowindowThirtyThree = new google.maps.InfoWindow({
  content: contentStringThirtyThree
});
var markerThirtyThree = new google.maps.Marker({
  position: myNumberThirtyThree,
  map: map,
  title: 'Бібліотека-філія №33'
});
google.maps.event.addListener(markerThirtyThree,'click', function() {
  closeClick();
  infowindowThirtyThree.open(map,markerThirtyThree);
});

/*ThirtyFour*/
var infowindowThirtyFour = new google.maps.InfoWindow({
  content: contentStringThirtyFour
});
var markerThirtyFour = new google.maps.Marker({
  position: myNumberThirtyFour,
  map: map,
  title: 'Бібліотека-філія №34/Винники/'
});
google.maps.event.addListener(markerThirtyFour,'click', function() {
  closeClick();
  infowindowThirtyFour.open(map,markerThirtyFour);
});

/*ThirtySeven*/
var infowindowThirtySeven = new google.maps.InfoWindow({
  content: contentStringThirtySeven
});
var markerThirtySeven = new google.maps.Marker({
  position: myNumberThirtySeven,
  map: map,
  title: 'Бібліотека-філія №37'
});
google.maps.event.addListener(markerThirtySeven,'click', function() {
  closeClick();
  infowindowThirtySeven.open(map,markerThirtySeven);
});

/*ThirtyEight*/
var infowindowThirtyEight = new google.maps.InfoWindow({
  content: contentStringThirtyEight
});
var markerThirtyEight = new google.maps.Marker({
  position: myNumberThirtyEight,
  map: map,
  title: 'Бібліотека-філія №38'
});
google.maps.event.addListener(markerThirtyEight,'click', function() {
  closeClick();
  infowindowThirtyEight.open(map,markerThirtyEight);
});

/*FourtyZero*/
var infowindowFourtyZero = new google.maps.InfoWindow({
  content: contentStringFourtyZero
});
var markerFourtyZero = new google.maps.Marker({
  position: myNumberFourtyZero,
  map: map,
  title: 'Бібліотека-філія №40'
});
google.maps.event.addListener(markerFourtyZero,'click', function() {
  closeClick();
  infowindowFourtyZero.open(map,markerFourtyZero);
});

/*FourtyOne*/
var infowindowFourtyOne = new google.maps.InfoWindow({
  content: contentStringFourtyOne
});
var markerFourtyOne = new google.maps.Marker({
  position: myNumberFourtyOne,
  map: map,
  title: 'Бібліотека-філія №41'
});
google.maps.event.addListener(markerFourtyOne,'click', function() {
  closeClick();
  infowindowFourtyOne.open(map,markerFourtyOne);
});

/*FourtyThree*/
var infowindowFourtyThree = new google.maps.InfoWindow({
  content: contentStringFourtyThree
});
var markerFourtyThree = new google.maps.Marker({
  position: myNumberFourtyThree,
  map: map,
  title: 'Бібліотека-філія №43'
});
google.maps.event.addListener(markerFourtyThree,'click', function() {
  closeClick();
  infowindowFourtyThree.open(map,markerFourtyThree);
});

/*FourtyFour*/
var infowindowFourtyFour = new google.maps.InfoWindow({
  content: contentStringFourtyFour
});
var markerFourtyFour = new google.maps.Marker({
  position: myNumberFourtyFour,
  map: map,
  title: 'Бібліотека-філія №44'
});
google.maps.event.addListener(markerFourtyFour,'click', function() {
  closeClick();
  infowindowFourtyFour.open(map,markerFourtyFour);
});



}

google.maps.event.addDomListener(window, 'load', initialize);

</script>

<!-- BEGIN #primary .hfeed-->
<div id="primary contacts_library" class="hfeed">

  <!-- BEGIN .hentry-->
  <div id="map-canvas"></div>

  <?php zilla_page_end(); ?>
  <!-- END .hentry-->
</div>

<div class="working-time">

  <span class="title">Години роботи: </span><br>
  Пн. - пт. 11:00-20:00<br>
  Неділя  11:00-17:00<br>
  Вихідний день-субота<br>
  Останній день місяця - санітарний<br>

</div>

<!-- END #primary .hfeed-->
</div>

<?php get_sidebar('page'); ?>

<?php get_footer(); ?>